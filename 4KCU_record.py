from pywinauto.application import Application
import subprocess
import pywinauto
import pyautogui
import time
import os, shutil
import fileinput
import cv2
import numpy as np
import PySimpleGUI as sg
from sys import exit
import sys

username = os.getlogin()
video_path_tmp = ''
appdata_path = 'C:\\Users\\'+username+'\\AppData\\Roaming\\Elgato\\4KCaptureUtility\\Settings.json'
######find location recording (video_path)
lines = open(appdata_path, 'r').readlines()
for counter in range(0, len(lines)-1):
    flag_setting = 0
    line_check = " ".join(lines[counter].split())
    if line_check == '"enableGOPCache":true,' :
        flag_setting = 1
    if flag_setting == 1:
        video_path_tmp = lines[counter+1]
        break

video_path_tmp = " ".join(video_path_tmp.split())
video_path_tmp = video_path_tmp.replace('"recordingFolder":"', '')
video_path_tmp = video_path_tmp.replace('",', '')
video_path = video_path_tmp
##########################################
#### find all mp4 in folder before recording
list_mp4_before = []
for root, dirs, files in os.walk(video_path):
    for file in files:
        if file.endswith(".mp4"):
            output_file=file
            paths = os.path.join(root, file)
            list_mp4_before += [paths]
####################################
def get_filepaths(directory):
    file_paths = []  
    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)  
    return file_paths  

def clear_folder(dir):    
    for filename in os.listdir(dir):
        file_path = os.path.join(dir, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

def replace_line(file_name, line_num, text):
    lines = open(file_name, 'r').readlines()
    lines[line_num] = text
    out = open(file_name, 'w')
    out.writelines(lines)
    out.close()
def apply_settings(path,f,h,w,f_n,h_n,w_n):
    replace_line(path, f, f_n)
    replace_line(path, h, h_n)
    replace_line(path, w, w_n)

lines = open(appdata_path, 'r').readlines()
number_lines = len(lines)
fps_change = number_lines - 20
height_change = number_lines - 7
width_change = number_lines - 6



layout = [  [sg.Text("Set record duration (Hour):")],     
            [sg.Input()],
            [sg.Text("Set record duration (Minute):")], 
            [sg.Input()],
            [sg.Text("Set record duration (Sencond):")], 
            [sg.Input()],
            [sg.Text("Choose output resolution:\n1.720p30\n2.720p60\n3.1080p30\n4.1080p60\n5.1440p30\n6.1440p60\n7.2160p30\n8.2160p60")],
            [sg.Input()],
            [sg.Button('Ok')] ]
window = sg.Window('Setup your Settings', layout) 
while True:
    event, values = window.read()                   
    if len(values[0]) == 0 :
        values[0] = '0'
    if len(values[1]) == 0 :
        values[1] = '0'
    if len(values[2]) == 0 :
        values[2] = '0'
    if len(values[3]) == 0 :
        values[3] = '0'   
    res = values[3]
    lenght_time = int(values[0]) + int(values[1]) + int(values[2])
    if lenght_time != 0 and int(res) != 0 :
        break
    if event == sg.WIN_CLOSED:
        window.close()
        exit()

window.close()
Hours_input = int(values[0])
Minute_input = int(values[1])
Sencond_input = int(values[2])
rec_duration=Hours_input*60*60 + Minute_input*60 + Sencond_input

if res == '1': 
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":30,\n','        "height":720,\n','        "width":1280\n')
    input_height=720
    input_width=1280
    input_fps=30
if res == '2':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":60,\n','        "height":720,\n','        "width":1280\n')
    input_height=720
    input_width=1280
    input_fps=60
if res == '3':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":30,\n','        "height":1080,\n','        "width":1920\n')
    input_height=1080
    input_width=1920
    input_fps=30
if res == '4':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":60,\n','        "height":1080,\n','        "width":1920\n')
    input_height=1080
    input_width=1920
    input_fps=60
if res == '5':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":30,\n','        "height":1440,\n','        "width":2560\n')
    input_height=1440
    input_width=2560
    input_fps=30
if res == '6':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":60,\n','        "height":1440,\n','        "width":2560\n')
    input_height=1440
    input_width=2560
    input_fps=60
if res == '7':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":30,\n','        "height":2160,\n','        "width":3840\n')
    input_height=2160
    input_width=3840
    input_fps=30
if res == '8':
    apply_settings(appdata_path,fps_change,height_change,width_change,'        "framerateFps":60,\n','        "height":2160,\n','        "width":3840\n')   
    input_height=2160
    input_width=3840
    input_fps=60
flash_back_ori = 'C:\\Users\\'+username+'\Videos\4KCapture\_Flashback.mp4'
time.sleep(1) 
print('On working....')
app=Application(backend='uia').start(r"C:\Program Files\Elgato\/4KCaptureUtility\/4KCaptureUtility.exe").connect(title='4K Capture Utility',timeout=100)

while True:
    check_flashback = get_filepaths(video_path)
    if len(check_flashback) != 0 :
        for root, dirs, files in os.walk(video_path):
            for file in files:
                if file.endswith(".mp4"):
                    str(file) == 'Flashback.mp4'
                    break
            break   
        break     
time.sleep(5) 

app=Application(backend='uia').connect(title='4K Capture Utility',timeout=100)
pyautogui.hotkey('ctrl','shift','r')
time.sleep(int(rec_duration))
app=Application(backend='uia').connect(title='4K Capture Utility',timeout=100)
pyautogui.hotkey('ctrl','shift','r')
time.sleep(5)

while True:
    check_flashback = get_filepaths(video_path)
    if len(check_flashback) != 0 :
        for root, dirs, files in os.walk(video_path):
            for file in files:
                if file.endswith(".mp4"):
                    str(file) == 'Flashback.mp4'
                    break
            break   
        break   
  
time.sleep(5)

app=Application(backend='uia').start(r"C:\Program Files\Elgato\/4KCaptureUtility\/4KCaptureUtility.exe").connect(title='4K Capture Utility',timeout=100)
pyautogui.hotkey('alt','F4')
time.sleep(5)
full_file_paths = ''
###### find all mp4 in folder after recording
list_mp4_after = []
for root, dirs, files in os.walk(video_path):
    for file in files:
        if file.endswith(".mp4"):
            output_file=file
            paths = os.path.join(root, file)
            list_mp4_after += [paths]
#############################################
###Compare to find mp4 recorded            
for counter in range(0, len(list_mp4_after) -1):
    flag = 1
    if list_mp4_after[counter] in list_mp4_before:
            flag = 0
    if flag == 1:
        full_file_paths = list_mp4_after[counter]
        break
################################               
video = cv2.VideoCapture(full_file_paths)

width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = int(video.get(cv2.CAP_PROP_FPS))

frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
duration = int(frame_count/fps)
hours = int(duration/3600)
minutes = int(((duration) -  hours*3600)/60)
seconds = int(duration - minutes*60 - hours*3600)
if width == input_width and height==input_height and fps == input_fps:
    results='PASS'
    color='Green'
else:
    results='FAIL'
    color='Red'
print(results)

layout = [[sg.Text('Format: '+str(input_height)+'p'+str(input_fps)+' Duration(H:M:S):'+str(Hours_input)+':'+str(Minute_input)+':'+str(Sencond_input))],
          [sg.Text("Output video res is: "+str(width)+'x'+str(height)+'p'+str(fps))],
          [sg.Text("Duration video (H:M:S): "+str(hours)+':'+ str(minutes)+':'+str(seconds))],
          [sg.Text(results,background_color=color)],
          [sg.Text(output_file)],
          [sg.Button('Open in file explorer')],
          [sg.Button("Close")]]
window = sg.Window("TEST RESULT", layout)

while True:
    event, values = window.read()
    if event == "Close" or event == sg.WIN_CLOSED:
        break
    if event=='Open in file explorer':
        subprocess.Popen(r'explorer /select,'+full_file_paths)
window.close()






